﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float speed;
    public static float intensity = 1f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, (speed * intensity) * Time.deltaTime); //= new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z + speed * Time.deltaTime);
    }
}
