﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door_script : MonoBehaviour
{
    public bool open = false;
    public SpriteRenderer spriteRenderer;
    public Sprite d_open;
    public Sprite d_closed;
    public GameObject door_closed;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (open)
        {
            gameObject.layer = LayerMask.NameToLayer("Door");
            door_closed.SetActive(false);
            //spriteRenderer.sprite = d_open;
        }
        else
        {
            gameObject.layer = LayerMask.NameToLayer("World");
            door_closed.SetActive(true);
            //spriteRenderer.sprite = d_closed;
        }
    }
}
