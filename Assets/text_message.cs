﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class text_message : MonoBehaviour
{
    public float DisplayTime = 0f;
    private GameObject player;
    public GameObject text;
    private bool follow = false;
    private Transform PlayerTransform;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        PlayerTransform = player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (follow)
        {
            transform.position = new Vector3(PlayerTransform.position.x, PlayerTransform.position.y + 0.5f, PlayerTransform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Clone")
        {
            text.SetActive(true);

            if (DisplayTime > 0)
            {
                follow = true;
                StartCoroutine(TimeToWait(DisplayTime));
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && (DisplayTime <= 0) || collision.gameObject.tag == "Clone" && (DisplayTime <= 0))
        {
            text.SetActive(false);
        }
    }

    IEnumerator TimeToWait(float sec)
    {
        yield return new WaitForSeconds(sec);
        Destroy(gameObject);
    }
}