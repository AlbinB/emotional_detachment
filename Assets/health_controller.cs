﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class health_controller : MonoBehaviour
{
    public float health = 10;
    public float maxHealth = 10;
    public bool canTakeDmg = true;
    public float immuneToDmgTime = 0.5f;
    private string current_scene;
    private GameObject PlayerObject;
    private bool runonce;
    public Transform checkpoint;
    // Start is called before the first frame update
    void Start()
    {
        PlayerObject = GameObject.Find("Player").gameObject;
        runonce = true;
        current_scene = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            if (gameObject.name == "Player" && runonce == true)
            {
                reload_scene();
                runonce = false;
            }
            else
            {
                gameObject.SetActive(false); // make function in clone_controller to diactivate 
                follow_smooth.follow = PlayerObject.transform;
                PlayerObject.GetComponent<clone_controller>().deactivate_filters();
                PlayerObject.GetComponent<PlayerMovementControls>().enabled = true;
                PlayerObject.GetComponent<clone_controller>().deactivate_fix();
            }
        }
    }
    public void addHealth(float add)
    {

        if (canTakeDmg)
        {
            health += add;

            canTakeDmg = false;
            StartCoroutine(Immune(immuneToDmgTime));
        }
    }
    IEnumerator Immune(float sec)
    {
        yield return new WaitForSeconds(sec);
        canTakeDmg = true;
    }
    void reload_scene()
    {
        SceneManager.LoadScene(current_scene);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Checkpoint")
        {
            checkpoint = collision.transform;
        }
    }

}
