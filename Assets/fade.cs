﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fade : MonoBehaviour
{
    public float minimum = 0.0f;
    public float maximum = 1f;
    public float duration = 5.0f;
    private float startTime;
    public SpriteRenderer sprite;
    public bool activated;
    private bool fix;

    // Start is called before the first frame update
    void Start()
    {
        //startTime = Time.time;
        activated = false;
        fix = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (activated)
        {
            if (fix)
            {
                startTime = Time.time;
                fix = false;
            }
            float t = (Time.time - startTime) / duration;
            sprite.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(maximum, minimum, t));
        }
    }
}
