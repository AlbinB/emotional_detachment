﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wheel_visual : MonoBehaviour
{
    public GameObject mouse;

    private SpriteRenderer spriteRenderer;

    public Sprite wizard;
    public Sprite pride;
    public Sprite anger;
    public Sprite ind;
    public Sprite curiosity;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mouse.GetComponent<Follow_mouse>().wizard == true)
        {
            spriteRenderer.sprite = wizard;
        }
        else if (mouse.transform.position.x < transform.position.x && mouse.transform.position.y > transform.position.y) // TOP LEFT
        {
            spriteRenderer.sprite = pride;
        }
        else if (mouse.transform.position.x > transform.position.x && mouse.transform.position.y > transform.position.y) // TOP RIGHT
        {
            spriteRenderer.sprite = anger;
        }
        else if (mouse.transform.position.x < transform.position.x && mouse.transform.position.y < transform.position.y) // BOTTOM LEFT
        {
            spriteRenderer.sprite = ind;
        }
        else if (mouse.transform.position.x > transform.position.x && mouse.transform.position.y < transform.position.y) // BOTTOM RIGHT
        {
            spriteRenderer.sprite = curiosity;
        }
    }
}
