﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow_mouse : MonoBehaviour
{
    private Vector3 target;
    public bool wizard;
    public GameObject wheel;
    private Transform wheel_pos;
    public float dist;
    public float wizard_dist;
    public SpriteRenderer mouse_sprite;

    // Start is called before the first frame update
    void Start()
    {
        wheel_pos = wheel.transform;
        mouse_sprite.enabled = false;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.position = new Vector3(target.x, target.y, transform.position.z);

        dist = Vector3.Distance(wheel_pos.position, transform.position);
        if (dist < wizard_dist)
        {
            wizard = true;
        }
        else
        {
            wizard = false;
        }
    }

}
