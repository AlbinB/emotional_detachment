﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clone_controller : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip emotion_spell;
    public AudioClip reset_spell;

    private Vector3 posPlayer;
    private Transform PlayerTransform;
    private GameObject PlayerObject;

    public static GameObject active_player;

    private GameObject pride_filter;
    private GameObject anger_filter;
    private GameObject indecisiveness_filter;
    private GameObject curiosity_filter;

    public int spawn_distance = 1;

    public KeyCode reset_key = KeyCode.Q;
    public KeyCode pride_key = KeyCode.Alpha1;
    public KeyCode indecisiveness_key = KeyCode.Alpha3;
    public KeyCode anger_key = KeyCode.Alpha2;
    public KeyCode curiosity_key = KeyCode.Alpha4;
    public KeyCode wheel_key = KeyCode.Mouse0;

    public GameObject curiosity_layer_visible;
    public GameObject curiosity_layer_invisible;
    public GameObject curiosity_objects_visible;
    public GameObject curiosity_objects_invisible;

    public GameObject selection_wheel;
    public GameObject mouse;

    public Animator animat;
    public string idle_2_animation;
    public string idle_animation;

    float timer;
    float holdDur = 1f;

    public GameObject Pride;
    public GameObject Indecisiveness;
    public GameObject Anger;
    public GameObject Curiosity;

    public static bool Pride_fix = false;           // I GIVE UP
    public static bool Indecisiveness_fix = false;  // I GIVE UP
    public static bool Anger_fix = false;           // I GIVE UP
    public static bool Curiosity_fix = false;       // I GIVE UP

    // Start is called before the first frame update
    void Start()
    {
        PlayerTransform = GameObject.Find("Player").transform;
        PlayerObject = GameObject.Find("Player").gameObject;
        active_player = PlayerObject;

        // Disable filters
        pride_filter = GameObject.Find("Pride_filter").gameObject;
        pride_filter.gameObject.SetActive(false);
        indecisiveness_filter = GameObject.Find("Indecisiveness_filter").gameObject;
        indecisiveness_filter.gameObject.SetActive(false);
        anger_filter = GameObject.Find("Anger_filter").gameObject;
        anger_filter.gameObject.SetActive(false);
        curiosity_filter = GameObject.Find("Curiosity_filter").gameObject;
        curiosity_filter.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(wheel_key))
        {
            selection_wheel.SetActive(true);

            mouse.GetComponent<Follow_mouse>().mouse_sprite.enabled = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            selection_wheel.SetActive(false);
            mouse.GetComponent<Follow_mouse>().mouse_sprite.enabled = false;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (Input.GetKeyUp(wheel_key))
        {
            if (mouse.GetComponent<Follow_mouse>().wizard == true)
            {
                // Disable other "players"
                deactivate_emotions();

                // Curiosity specefic layer
                curiosity_vision_off();

                PlayerObject.GetComponent<PlayerMovementControls>().enabled = true;

                //deactivate_filters();
                deactivate_filters();
                follow_smooth.follow = PlayerTransform;
                Rotator.intensity = 1f;

                deactivate_fix();

                activate_player();

                audioSource.PlayOneShot(reset_spell, 0.5F);
            }
            else
            {
                if (mouse.transform.position.x < selection_wheel.transform.position.x && mouse.transform.position.y > selection_wheel.transform.position.y) // TOP LEFT
                {
                    animat.Play(idle_2_animation);

                    Anger.SetActive(false);
                    Indecisiveness.SetActive(false);
                    Curiosity.SetActive(false);

                    // Windmill intensity
                    Rotator.intensity = 2.5f;

                    // Filters
                    deactivate_filters();
                    pride_filter.gameObject.SetActive(true);

                    // If Pride is disabled
                    if (!Pride.activeInHierarchy)
                    {
                        // Player position
                        posPlayer = gameObject.transform.position;

                        // Activate Pride and move to player
                        Pride.SetActive(true);
                        Pride.GetComponent<health_controller>().health = 1;
                        Pride.GetComponent<health_controller>().canTakeDmg = true;
                        Pride.transform.position = new Vector3(posPlayer.x, posPlayer.y, Pride.transform.position.z);
                    }
                    // Camera follow
                    follow_smooth.follow = Pride.transform;

                    // Disable other "players"
                    deactivate_emotions();

                    // Curiosity specefic layer
                    curiosity_vision_off();

                    // Enable Pride movment
                    Pride.GetComponent<PlayerMovementControls>().enabled = true;

                    deactivate_fix();
                    Pride_fix = true;
                    active_player = Pride;
                    audioSource.PlayOneShot(emotion_spell, 0.5F);
                }
                else if (mouse.transform.position.x > selection_wheel.transform.position.x && mouse.transform.position.y > selection_wheel.transform.position.y) // TOP RIGHT
                {
                    animat.Play(idle_2_animation);

                    Pride.SetActive(false);
                    Indecisiveness.SetActive(false);
                    Curiosity.SetActive(false);

                    Rotator.intensity = 20f;
                    // Filters
                    deactivate_filters();
                    anger_filter.gameObject.SetActive(true);

                    // If Anger is disabled
                    if (!Anger.activeInHierarchy)
                    {
                        // Player position
                        posPlayer = gameObject.transform.position;

                        // Activate Anger and move to player
                        Anger.SetActive(true);
                        Anger.GetComponent<health_controller>().health = 3;
                        Anger.GetComponent<health_controller>().canTakeDmg = true;
                        Anger.transform.position = new Vector3(posPlayer.x, posPlayer.y, Anger.transform.position.z);
                    }
                    // Camera follow
                    follow_smooth.follow = Anger.transform;

                    // Disable other "players"
                    deactivate_emotions();

                    // Curiosity specefic layer
                    curiosity_vision_off();

                    // Enable Anger movment
                    Anger.GetComponent<PlayerMovementControls>().enabled = true;

                    deactivate_fix();
                    Anger_fix = true;

                    active_player = Anger;
                    audioSource.PlayOneShot(emotion_spell, 0.5F);
                }
                else if (mouse.transform.position.x < selection_wheel.transform.position.x && mouse.transform.position.y < selection_wheel.transform.position.y) // BOTTOM LEFT
                {
                    animat.Play(idle_2_animation);

                    Pride.SetActive(false);
                    Anger.SetActive(false);
                    Curiosity.SetActive(false);

                    Rotator.intensity = 0f;
                    // Filters
                    deactivate_filters();
                    indecisiveness_filter.gameObject.SetActive(true);

                    // If Indecisiveness is disabled
                    if (!Indecisiveness.activeInHierarchy)
                    {
                        // Player position
                        posPlayer = gameObject.transform.position;

                        // Activate Indecisiveness and move to player
                        Indecisiveness.SetActive(true);
                        Indecisiveness.GetComponent<health_controller>().health = 3;
                        Indecisiveness.GetComponent<health_controller>().canTakeDmg = true;
                        Indecisiveness.transform.position = new Vector3(posPlayer.x, posPlayer.y, Indecisiveness.transform.position.z);
                    }
                    // Camera follow
                    follow_smooth.follow = Indecisiveness.transform;

                    // Disable other "players"
                    deactivate_emotions();

                    // Curiosity specefic layer
                    curiosity_vision_off();

                    // Enable Indecisiveness movment
                    Indecisiveness.GetComponent<PlayerMovementControls>().enabled = true;
                    deactivate_fix();
                    Indecisiveness_fix = true;
                    active_player = Indecisiveness;
                    audioSource.PlayOneShot(emotion_spell, 0.5F);
                }
                else if (mouse.transform.position.x > selection_wheel.transform.position.x && mouse.transform.position.y < selection_wheel.transform.position.y) // BOTTOM RIGHT
                {
                    animat.Play(idle_2_animation);

                    Pride.SetActive(false);
                    Anger.SetActive(false);
                    Indecisiveness.SetActive(false);

                    Rotator.intensity = -2.5f;
                    // Filters
                    deactivate_filters();
                    curiosity_filter.gameObject.SetActive(true);


                    // If Curiosity is disabled
                    if (!Curiosity.activeInHierarchy)
                    {
                        // Player position
                        posPlayer = gameObject.transform.position;

                        // Activate Curiosity and move to player
                        Curiosity.SetActive(true);
                        Curiosity.GetComponent<health_controller>().health = 3;
                        Curiosity.GetComponent<health_controller>().canTakeDmg = true;
                        Curiosity.transform.position = new Vector3(posPlayer.x, posPlayer.y, Curiosity.transform.position.z);

                    }
                    // Camera follow
                    follow_smooth.follow = Curiosity.transform;

                    // Disable other "players"
                    deactivate_emotions();

                    // Curiosity specefic layer
                    curiosity_vision_on();

                    // Enable Curiosity movment
                    Curiosity.GetComponent<PlayerMovementControls>().enabled = true;

                    deactivate_fix();
                    Curiosity_fix = true;

                    active_player = Curiosity;
                    audioSource.PlayOneShot(emotion_spell, 0.5F);
                }

            }

        }
        if (Input.GetKeyDown(reset_key))
        {
            timer = Time.time;
        }
        else if (Input.GetKey(reset_key))
        {
            if (Time.time - timer > holdDur)
            {
                //by making it positive inf, we won't subsequently run this code by accident,
                //since X - +inf = -inf, which is always less than holdDur
                timer = float.PositiveInfinity;


                //perform your action
                deactivate_emotions();
                PlayerObject.GetComponent<PlayerMovementControls>().enabled = true;

                deactivate_filters();
                follow_smooth.follow = PlayerTransform;
                Rotator.intensity = 1f;

                deactivate_fix();

                Pride.SetActive(false);
                Anger.SetActive(false);
                Indecisiveness.SetActive(false);
                Curiosity.SetActive(false);
            }
        }
        else
        {
            timer = float.PositiveInfinity;
        }
        /*
        if (PlayerMovementControls.faceingRight == true)
        {
            spawn_distance = 1;
        }
        else
        {
            spawn_distance = -1;
        }*/
        if (Input.GetKeyDown(reset_key))
        {
            // Disable other "players"
            
            deactivate_emotions();
            PlayerObject.GetComponent<PlayerMovementControls>().enabled = true;

            deactivate_filters();
            follow_smooth.follow = PlayerTransform;
            Rotator.intensity = 1f;

            curiosity_vision_off();

            deactivate_fix();

            activate_player();
            audioSource.PlayOneShot(reset_spell, 0.5F);
        }

        if (Input.GetKeyDown(pride_key))
        {
            animat.Play(idle_2_animation);

            Anger.SetActive(false);
            Indecisiveness.SetActive(false);
            Curiosity.SetActive(false);

            // Windmill intensity
            Rotator.intensity = 2.5f;

            // Filters
            deactivate_filters();
            pride_filter.gameObject.SetActive(true);

            // If Pride is disabled
            if (!Pride.activeInHierarchy) 
            {
                // Player position
                posPlayer = gameObject.transform.position;

                // Activate Pride and move to player
                Pride.SetActive(true);
                Pride.GetComponent<health_controller>().health = 1;
                Pride.GetComponent<health_controller>().canTakeDmg = true;
                Pride.transform.position = new Vector3(posPlayer.x, posPlayer.y, Pride.transform.position.z);
            }
            // Camera follow
            follow_smooth.follow = Pride.transform;

            // Disable other "players"
            deactivate_emotions();

            // Curiosity specefic layer
            curiosity_vision_off();

            // Enable Pride movment
            Pride.GetComponent<PlayerMovementControls>().enabled = true;

            deactivate_fix();
            Pride_fix = true;
            active_player = Pride;

            //Trying to move new emotion forward
            /*
            rb = current_pride.GetComponent<Rigidbody2D>();
            rb.AddForce(transform.forward);
            */
            audioSource.PlayOneShot(emotion_spell, 0.5F);
        }
        if (Input.GetKeyDown(indecisiveness_key))
        {
            animat.Play(idle_2_animation);

            Pride.SetActive(false);
            Anger.SetActive(false);
            Curiosity.SetActive(false);

            Rotator.intensity = 0f;
            // Filters
            deactivate_filters();
            indecisiveness_filter.gameObject.SetActive(true);

            // If Indecisiveness is disabled
            if (!Indecisiveness.activeInHierarchy)
            {
                // Player position
                posPlayer = gameObject.transform.position;

                // Activate Indecisiveness and move to player
                Indecisiveness.SetActive(true);
                Indecisiveness.GetComponent<health_controller>().health = 3;
                Indecisiveness.GetComponent<health_controller>().canTakeDmg = true;
                Indecisiveness.transform.position = new Vector3(posPlayer.x, posPlayer.y, Indecisiveness.transform.position.z);
            }
            // Camera follow
            follow_smooth.follow = Indecisiveness.transform;

            // Disable other "players"
            deactivate_emotions();

            // Curiosity specefic layer
            curiosity_vision_off();

            // Enable Indecisiveness movment
            Indecisiveness.GetComponent<PlayerMovementControls>().enabled = true;
            deactivate_fix();
            Indecisiveness_fix = true;
            active_player = Indecisiveness;
            audioSource.PlayOneShot(emotion_spell, 0.5F);
        }

        if (Input.GetKeyDown(anger_key))
        {
            animat.Play(idle_2_animation);

            Pride.SetActive(false);
            Indecisiveness.SetActive(false);
            Curiosity.SetActive(false);

            Rotator.intensity = 20f;
            // Filters
            deactivate_filters();
            anger_filter.gameObject.SetActive(true);

            // If Anger is disabled
            if (!Anger.activeInHierarchy)
            {
                // Player position
                posPlayer = gameObject.transform.position;

                // Activate Anger and move to player
                Anger.SetActive(true);
                Anger.GetComponent<health_controller>().health = 3;
                Anger.GetComponent<health_controller>().canTakeDmg = true;
                Anger.transform.position = new Vector3(posPlayer.x, posPlayer.y, Anger.transform.position.z);
            }
            // Camera follow
            follow_smooth.follow = Anger.transform;

            // Disable other "players"
            deactivate_emotions();

            // Curiosity specefic layer
            curiosity_vision_off();

            // Enable Anger movment
            Anger.GetComponent<PlayerMovementControls>().enabled = true;

            deactivate_fix();
            Anger_fix = true;

            active_player = Anger;
            audioSource.PlayOneShot(emotion_spell, 0.5F);
        }

        if (Input.GetKeyDown(curiosity_key))
        {
            animat.Play(idle_2_animation);

            Pride.SetActive(false);
            Anger.SetActive(false);
            Indecisiveness.SetActive(false);

            Rotator.intensity = -2.5f;
            // Filters
            deactivate_filters();
            curiosity_filter.gameObject.SetActive(true);

            // If Curiosity is disabled
            if (!Curiosity.activeInHierarchy)
            {
                // Player position
                posPlayer = gameObject.transform.position;

                // Activate Curiosity and move to player
                Curiosity.SetActive(true);
                Curiosity.GetComponent<health_controller>().health = 3;
                Curiosity.GetComponent<health_controller>().canTakeDmg = true;
                Curiosity.transform.position = new Vector3(posPlayer.x, posPlayer.y, Curiosity.transform.position.z);

            }
            // Camera follow
            follow_smooth.follow = Curiosity.transform;

            // Disable other "players"
            deactivate_emotions();

            // Curiosity specefic layer
            curiosity_vision_on();

            // Enable Curiosity movment
            Curiosity.GetComponent<PlayerMovementControls>().enabled = true;

            deactivate_fix();
            Curiosity_fix = true;
            
            active_player = Curiosity;
            audioSource.PlayOneShot(emotion_spell, 0.5F);
        }
    }

    public void deactivate_filters()
    {
        pride_filter.gameObject.SetActive(false);
        indecisiveness_filter.gameObject.SetActive(false);
        anger_filter.gameObject.SetActive(false);
        curiosity_filter.gameObject.SetActive(false);
    }
    public void deactivate_emotions()
    {
        PlayerObject.GetComponent<PlayerMovementControls>().enabled = false;
        if (Pride != null)
        {
            Pride.GetComponent<PlayerMovementControls>().anim.Play(Pride.GetComponent<PlayerMovementControls>().idle_animation);
            Pride.GetComponent<PlayerMovementControls>().enabled = false;
        }
        if (Indecisiveness != null)
        {
            Indecisiveness.GetComponent<PlayerMovementControls>().anim.Play(Indecisiveness.GetComponent<PlayerMovementControls>().idle_animation);
            Indecisiveness.GetComponent<PlayerMovementControls>().enabled = false;
        }
        if (Anger != null)
        {
            Anger.GetComponent<PlayerMovementControls>().anim.Play(Anger.GetComponent<PlayerMovementControls>().idle_animation);
            Anger.GetComponent<PlayerMovementControls>().enabled = false;
        }
        if (Curiosity != null)
        {
            Curiosity.GetComponent<PlayerMovementControls>().anim.Play(Curiosity.GetComponent<PlayerMovementControls>().idle_animation);
            Curiosity.GetComponent<PlayerMovementControls>().enabled = false;
        }
    }

    public void activate_player()
    {
        active_player = PlayerObject;
        animat.Play(idle_animation);
    }

    private void curiosity_vision_off()
    {
        // Curiosity specefic layers and objects
        if (curiosity_layer_invisible != null)
        {
            curiosity_layer_invisible.SetActive(false);
        }
        if (curiosity_layer_visible != null)
        {
            curiosity_layer_visible.SetActive(true);
        }
        if (curiosity_objects_invisible != null)
        {
            curiosity_objects_invisible.SetActive(false);
        }
        if (curiosity_objects_visible != null)
        {
            curiosity_objects_visible.SetActive(true);
        }
    }
    private void curiosity_vision_on()
    {
        // Curiosity specefic layers and objects
        if (curiosity_layer_invisible != null)
        {
            curiosity_layer_invisible.SetActive(true);
        }
        if (curiosity_layer_visible != null)
        {
            curiosity_layer_visible.SetActive(false);
        }
        if (curiosity_objects_invisible != null)
        {
            curiosity_objects_invisible.SetActive(true);
        }
        if (curiosity_objects_visible != null)
        {
            curiosity_objects_visible.SetActive(false);
        }
    }


    public void deactivate_fix()
    {
        Pride_fix = false;
        Indecisiveness_fix = false;
        Anger_fix = false;
        Curiosity_fix = false;
    }
}