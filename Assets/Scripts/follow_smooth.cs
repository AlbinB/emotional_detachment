﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class follow_smooth : MonoBehaviour
{

    public static Transform follow;
    public float yoffset = 0f;
    private float dist;


    // Start is called before the first frame update
    void Start()
    {
        follow = GameObject.Find("Player").transform;
        transform.position = new Vector3(follow.position.x, follow.position.y + yoffset, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(follow.position.x, follow.position.y + yoffset, transform.position.z), 5 * Time.smoothDeltaTime);
    }
}
