﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float speed = 100;
    //private bool returnToStart = false;
    //private bool activatable = true;
    private Vector3 startPos;
    public Transform endPos;
    public bool activated = false;
    Vector3 targetPos;
    private Collider2D col;
    private Vector3 scaleChange;
    public bool turn_around = false;

    void Start()
    {
        startPos = transform.position;
        col = GetComponent<Collider2D>();
        targetPos = startPos;
    }


    void Update()
    {

        if (activated)
        {
            if (transform.position == startPos)
            {
                targetPos = endPos.position;
            }
            else if (transform.position == endPos.position)
            {
                targetPos = startPos;
            }
        }

        if (activated)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        }
    }
    //draws line between pos1 and pos2
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, endPos.position);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {

        //collision.gameObject.transform.SetParent(transform);
        
        if (collision.transform.position.y > col.transform.position.y)
        {
            //contactCol = true;
            collision.gameObject.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.activeSelf == true)
        {
            collision.gameObject.transform.SetParent(null);
        }
    }
}
