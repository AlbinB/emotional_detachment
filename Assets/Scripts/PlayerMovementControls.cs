﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementControls : MonoBehaviour
{
    // Start is called before the first frame update

    public AudioSource audioSource;
    public AudioClip jump_sound;
    public AudioClip land_sound;

    public bool active = true;

    private bool canplayland = true;

    Rigidbody2D rb;
    private Vector2 moveVelocity;
    public float groundRayDistance = 1.7f;
    public static bool faceingRight = true;

    public LayerMask layerM;
    public Transform feetDetect;
    public KeyCode jumpKey = KeyCode.Space;
    public static KeyCode usekey = KeyCode.E;
    public bool canDoubleJump;
    private bool hasDoubleJumped;
    public bool canMoveVertical = false;
    public float speed = 50;
    public float jumpVelocity = 50;
    public bool playingJumpAnim = false;
    public bool dash_equipped;

    public string run_animation;
    public string idle_animation;
    public string jump_animation;
    public string fall_animation;

    private bool canDash = true;
    public KeyCode dashKey = KeyCode.LeftShift;
    public float dashVelocity = 40;
    public float dashCooldown = 2;
    public float dashDuration = 0.2f;
    public bool isDashing = false;

    public Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        if (feetDetect)
        {
            groundRayDistance = Mathf.Abs(Mathf.Sqrt(Mathf.Pow((transform.position.x - feetDetect.position.x), 2) + Mathf.Pow((transform.position.y - feetDetect.position.y), 2)));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (active == true)
        {
            CheckFacingDirection();

            if (Input.GetKeyDown(jumpKey))
                Jump();

            Move();

            void Move()
            {
                Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                if (moveInput != new Vector2(0, 0))
                {
                    if (canMoveVertical)
                    {
                        moveVelocity = moveInput.normalized * speed;
                    }
                    else
                    {
                        moveVelocity = new Vector2(moveInput.normalized.x * speed, rb.velocity.y);
                    }
                    rb.velocity = moveVelocity;
                    if (IsGroundUnder() && !isDashing && !playingJumpAnim)
                    {
                        playAnim(run_animation);
                    }
                }
                else
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);

                    if (IsGroundUnder() && !isDashing && !playingJumpAnim)
                    {
                        playAnim(idle_animation);
                    }
                }

            }


            if (dash_equipped && Input.GetKeyDown(dashKey))
            {
                if (canDash)
                {
                    isDashing = true;
                    canDash = false;
                    StartCoroutine(dashTimeOut());
                }
            }
            DashMovement();

            if (IsGroundUnder())
            {
                hasDoubleJumped = false;
                playingJumpAnim = false;
            }
            else //if(!playingJumpAnim)
            {
                if (!isDashing)
                {
                    if (rb.velocity.y > 0)
                    {
                        playAnim(jump_animation);
                    }
                    else
                    {
                        playAnim(fall_animation);
                    }
                    //playingJumpAnim = true;
                }
            }

            if (IsGroundUnder() && (!isDashing))
            {
                canDash = true;
            }
        }

    IEnumerator dashTimeOut()
    {
        yield return new WaitForSeconds(dashDuration);
        isDashing = false;
        //StartCoroutine(CooldownDash(dashCooldown));
    }

    void DashMovement()
    {
        if (isDashing)
        {
            playAnim(idle_animation);
            if (faceingRight)
            {
                rb.velocity = new Vector2(dashVelocity, 0);
            }
            else
            {
                rb.velocity = new Vector2(-dashVelocity, 0);
            }
            rb.velocity = new Vector2(rb.velocity.x, jumpVelocity / 20);
        }
    }

    bool IsGroundUnder()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, groundRayDistance, layerM);
            if (hit.collider)
            {
                if (canplayland)
                {
                    audioSource.PlayOneShot(land_sound, 0.5F);
                    canplayland = false;
                }
            return true;
        }
        canplayland = true;
        return false;
    }
    void Jump()
    {
        if (IsGroundUnder())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpVelocity);
            audioSource.PlayOneShot(jump_sound, 0.5F);
        }
        else if (canDoubleJump)
        {
            if (!hasDoubleJumped)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpVelocity);
                hasDoubleJumped = true;
            }
        }
    }

    void playAnim(string _anim)
    {
        if (anim)
        {
            anim.Play(_anim);
        }
    }
    /*
    void Dash()
    {
        if (Input.GetKeyDown(dashKey) && canDash)
        {
            canDash = false;
            //StartCoroutine(CooldownDash(dashCooldown));

            if (faceingRight)
            {
                rb.AddForce(-Vector2.left * dashVelocity, ForceMode2D.Impulse);
            }
            else
            {
                rb.AddForce(Vector2.left * dashVelocity, ForceMode2D.Impulse);
            }
        }
    }*/

    void CheckFacingDirection()
    {
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            if (!faceingRight)
            {
                faceingRight = true;
            }
        }
        else if (Input.GetAxisRaw("Horizontal") != 0)
        {
            if (faceingRight)
            {
                faceingRight = false;
            }
        }
    }

        if ((faceingRight) && (transform.localScale.x < 0))
        {
            transform.localScale = new Vector3(0.25f, transform.localScale.y, transform.localScale.z);
        }
        else if ((!faceingRight) && (transform.localScale.x > 0))
        {
            transform.localScale = new Vector3(-0.25f, transform.localScale.y, transform.localScale.z);
        }
        /*
    void FlipDirection()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }*/
        /*
            IEnumerator CooldownDash(float sec)
            {
                yield return new WaitForSeconds(sec);
                canDash = true;
            }*/
    }
}

