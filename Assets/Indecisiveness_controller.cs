﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indecisiveness_controller : MonoBehaviour
{

    public KeyCode SwitchKey = KeyCode.Tab;
    private GameObject PlayerObject;
    private Transform PlayerTransform;
    private Vector3 previous_position;

    public AudioSource audioSource;
    public AudioClip swap_sound;

    public float distance;

    // Start is called before the first frame update
    void Start()
    {
        PlayerObject = GameObject.Find("Player").gameObject;
        PlayerTransform = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(SwitchKey))
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up);
            // If it hits something...
            if (hit.collider != null)
            {
                // Calculate the distance from the surface and the "error" relative
                distance = Mathf.Abs(hit.point.y - transform.position.y);
            }
            else
            {
                distance = 3;
            }
            if (distance > 2)
            {
                previous_position = new Vector3(transform.position.x, transform.position.y, transform.position.z);                  //Save old position
                PlayerTransform = GameObject.Find("Player").transform;                                                              //Save player position
                transform.position = new Vector3(PlayerTransform.position.x, PlayerTransform.position.y, transform.position.z);     //Move to player position
                PlayerObject.transform.position = previous_position;                                                                // Move player to old position

                follow_smooth.follow = PlayerObject.transform;
                PlayerObject.GetComponent<clone_controller>().deactivate_emotions();
                PlayerObject.GetComponent<clone_controller>().deactivate_filters();
                PlayerObject.GetComponent<clone_controller>().deactivate_fix();
                PlayerObject.GetComponent<PlayerMovementControls>().enabled = true;
                PlayerObject.GetComponent<clone_controller>().activate_player();

                Rotator.intensity = 1f;
                audioSource.PlayOneShot(swap_sound, 0.5F);
            }
        }
    }
}
