﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hazard_damage : MonoBehaviour
{

    public float damage = 1;
    public Collision2D col;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        health_controller temp = collision.gameObject.GetComponent<health_controller>();
        if (temp != null)
            temp.addHealth(-damage);
    }

}
