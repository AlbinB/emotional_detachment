﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destructible_wall : MonoBehaviour
{

    public GameObject destroyed_wall;
    private float distance;
    private Vector3 pos;
    private Vector3 scale;
    private GameObject wall;
    private GameObject Anger;


    // Start is called before the first frame update
    void Start()
    {

    }
    
    // Update is called once per frame
    void Update()
    {
        if (clone_controller.Anger_fix)
        {
            Anger = GameObject.Find("Anger").gameObject;
            if (Anger.activeInHierarchy)
            { 
                distance = Vector3.Distance(gameObject.transform.position, Anger.transform.position);
                if (distance < 4.5)
                {
                
                    pos = gameObject.transform.position;
                    scale = gameObject.transform.localScale;
                    wall = Instantiate(destroyed_wall, new Vector3(pos.x, pos.y, 0), Quaternion.identity);
                    wall.transform.localScale = new Vector3(scale.x, scale.y, scale.z); // change its local scale in x y z format
                    Destroy(gameObject);
                }
            }
        }
    }
}
