﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class intro_start : MonoBehaviour
{

    public GameObject[] slides; // Array (list) of all the moving platforms to be controlled
    public AudioClip[] sounds;
    public AudioSource audioSource;
    public KeyCode activate_key;
    private int i;
    public int end_slide;
    public string next_scene_name;

    // Start is called before the first frame update
    void Start()
    {
        i = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(activate_key))
        {
            if (i == end_slide)
            {
                SceneManager.LoadScene(next_scene_name);
            }
            else
            {
                if (slides[i] != null)
                {
                    slides[i].GetComponent<fade>().activated = true;
                }
                if (sounds[i] != null)
                {
                    audioSource.Stop();
                    audioSource.volume = 0.25F;
                    audioSource.PlayOneShot(sounds[i], 1F);
                }
                i += 1;
            }
        }
    }
}
