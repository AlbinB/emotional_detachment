﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class load_next_level : MonoBehaviour
{
    //public Object next_scene;
    public string next_scene_name;

    // Start is called before the first frame update
    void Start()
    {
        //next_scene_name = next_scene.name;
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(next_scene_name);
        }
    }
}