﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpoint : MonoBehaviour
{

    public Sprite activated;
    public Sprite unactivated;
    private SpriteRenderer spriteI;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player = collision.gameObject;
            if (player.GetComponent<health_controller>().health < player.GetComponent<health_controller>().maxHealth)
            {
                /*
                for (float i = player.GetComponent<health_controller>().health; i < player.GetComponent<health_controller>().maxHealth; i++)
                {
                    playerHealthUI.addHealth(Mathf.Abs((int)1));
                }*/
                player.GetComponent<health_controller>().health = player.GetComponent<health_controller>().maxHealth;
            }

            GameObject[] checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint");
            foreach (GameObject Checkpoint in checkpoints)
            {
                spriteI = Checkpoint.GetComponent<SpriteRenderer>();
                spriteI.sprite = unactivated;
            }

            GetComponent<SpriteRenderer>().sprite = activated;
        }
    }

}
