﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lever_script : MonoBehaviour
{
    public GameObject[] doors; // Array (list) of all the doors to be controlled
    public GameObject[] platforms; // Array (list) of all the moving platforms to be controlled
    private Vector3 pos;
    private Vector3 scale;
    public bool contact = false;

    public AudioSource audioSource;
    public AudioClip lever_sound;

    private GameObject PlayerObject;
    private Component clone;

    public bool active = false;
    private SpriteRenderer spriteRenderer;
    public Sprite l_active;
    public Sprite l_inactive;


    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        PlayerObject = GameObject.Find("Player").gameObject;
        clone = PlayerObject.GetComponent<clone_controller>();
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            spriteRenderer.sprite = l_active;
        }
        else
        {
            spriteRenderer.sprite = l_inactive;
        }

        
        if (!clone_controller.Indecisiveness_fix)
        {
            if (contact && Input.GetKeyDown(PlayerMovementControls.usekey))
            {
                if (active)
                {
                    active = false;
                    door_handling();
                    platform_handling();
                }
                else
                {
                    active = true;
                    door_handling();
                    platform_handling();
                }
                audioSource.PlayOneShot(lever_sound, 0.5F);
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        contact = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        contact = false;
    }

    void door_handling()
    {
        for (int i = 0; i < doors.Length; i++)
        {
            if (doors[i].GetComponent<door_script>().open == true)
            {
                doors[i].GetComponent<door_script>().open = false;
            }
            else
            {
                doors[i].GetComponent<door_script>().open = true;
            }
        }
    }

    void platform_handling()
    {
        for (int i = 0; i < platforms.Length; i++)
        {
            if (platforms[i].GetComponent<MovingPlatform>().activated == true)
            {
                platforms[i].GetComponent<MovingPlatform>().activated = false;
            }
            else
            {
                platforms[i].GetComponent<MovingPlatform>().activated = true;
            }
        }
    }
}