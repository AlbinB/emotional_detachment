<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="reference" tilewidth="270" tileheight="383" tilecount="18" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <image width="110" height="219" source="Tiled/Anger.png"/>
 </tile>
 <tile id="2">
  <image width="104" height="173" source="Tiled/Curiosity.png"/>
 </tile>
 <tile id="3">
  <image width="70" height="131" source="Tiled/Indecisiveness.png"/>
 </tile>
 <tile id="4">
  <image width="92" height="216" source="Tiled/Pride.png"/>
 </tile>
 <tile id="5">
  <image width="72" height="189" source="Tiled/Wizard_of_feels.png"/>
 </tile>
 <tile id="6">
  <image width="188" height="188" source="Tiled/Lever_On.png"/>
 </tile>
 <tile id="9">
  <image width="250" height="250" source="Tiled/Door_Closed.png"/>
 </tile>
 <tile id="10">
  <image width="250" height="250" source="Tiled/Door_Opened.png"/>
 </tile>
 <tile id="11">
  <image width="156" height="149" source="Tiled/F8DE61285EFE46CAAAA587DCEA07CB80.png"/>
 </tile>
 <tile id="14">
  <image width="128" height="128" source="Tiled/wooden-sign.png"/>
 </tile>
 <tile id="27">
  <image width="199" height="180" source="Tiled/Box.png"/>
 </tile>
 <tile id="29">
  <image width="270" height="353" source="Tiled/Destructible_wall.png"/>
 </tile>
 <tile id="36">
  <image width="62" height="135" source="Tiled/Glass 1.png"/>
 </tile>
 <tile id="37">
  <image width="121" height="173" source="Tiled/Glass 2.png"/>
 </tile>
 <tile id="38">
  <image width="111" height="118" source="Tiled/Glass 3.png"/>
 </tile>
 <tile id="39">
  <image width="49" height="82" source="Tiled/Glass 4.png"/>
 </tile>
 <tile id="40">
  <image width="92" height="205" source="Tiled/Glass 5.png"/>
 </tile>
 <tile id="41">
  <image width="250" height="383" source="Tiled/Goal.png"/>
 </tile>
</tileset>
