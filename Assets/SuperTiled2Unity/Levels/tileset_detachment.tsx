<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="tileset_detachment" tilewidth="64" tileheight="64" tilecount="24" columns="6">
 <image source="tileset_detachment.png" width="384" height="256"/>
 <tile id="1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="64">
    <polygon points="0,0 62,-63 62,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="32">
    <polygon points="0,0 0,32 -61,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0" y="32">
    <polygon points="0,0 19,-7 25,-13 45,-22 58,-29 64,-32 64,32 0,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="2" width="64" height="62"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="3" width="64" height="61"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3" y="64">
    <polygon points="0,0 10,-6 31,-10 46,-17 61,-18 61,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="46">
    <polygon points="0,0 32,-13 43,-15 58,-23 64,-24 64,18 0,18"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="2">
    <polygon points="0,0 -7,0 -26,8 -44,12 -60,19 -64,19 -64,62 0,62"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="0">
    <polygon points="0,0 -64,0 -64,64 0,64"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="2" width="64" height="62"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="2" width="64" height="62"/>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="50">
    <polygon points="0,0 -22,5 -38,9 -61,14 0,14"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="35">
    <polygon points="0,0 -32,7 -39,8 -53,14 -64,15 -64,29 0,29"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="19">
    <polygon points="0,0 -17,5 -41,8 -62,15 -64,15 -64,45 0,45"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="64" y="2">
    <polygon points="0,0 -63,16 -64,16 -64,62 0,62"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="2" width="64" height="62"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="2" width="64" height="62"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="2" width="62" height="62"/>
  </objectgroup>
 </tile>
 <tile id="19">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="2" width="62" height="62"/>
  </objectgroup>
 </tile>
</tileset>
