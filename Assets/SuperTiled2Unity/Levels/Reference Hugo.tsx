<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="Reference Hugo" tilewidth="270" tileheight="383" tilecount="18" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="12">
  <image width="110" height="219" source="Tiled/Anger.png"/>
 </tile>
 <tile id="13">
  <image width="199" height="180" source="Tiled/Box.png"/>
 </tile>
 <tile id="14">
  <image width="104" height="173" source="Tiled/Curiosity.png"/>
 </tile>
 <tile id="15">
  <image width="270" height="353" source="Tiled/Destructible_wall.png"/>
 </tile>
 <tile id="16">
  <image width="250" height="250" source="Tiled/Door_Closed.png"/>
 </tile>
 <tile id="17">
  <image width="250" height="250" source="Tiled/Door_Opened.png"/>
 </tile>
 <tile id="18">
  <image width="156" height="149" source="Tiled/F8DE61285EFE46CAAAA587DCEA07CB80.png"/>
 </tile>
 <tile id="19">
  <image width="70" height="131" source="Tiled/Indecisiveness.png"/>
 </tile>
 <tile id="20">
  <image width="188" height="188" source="Tiled/Lever_On.png"/>
 </tile>
 <tile id="21">
  <image width="92" height="216" source="Tiled/Pride.png"/>
 </tile>
 <tile id="22">
  <image width="72" height="189" source="Tiled/Wizard_of_feels.png"/>
 </tile>
 <tile id="23">
  <image width="128" height="128" source="Tiled/wooden-sign.png"/>
 </tile>
 <tile id="24">
  <image width="62" height="135" source="Tiled/Glass 1.png"/>
 </tile>
 <tile id="25">
  <image width="121" height="173" source="Tiled/Glass 2.png"/>
 </tile>
 <tile id="26">
  <image width="111" height="118" source="Tiled/Glass 3.png"/>
 </tile>
 <tile id="27">
  <image width="49" height="82" source="Tiled/Glass 4.png"/>
 </tile>
 <tile id="28">
  <image width="92" height="205" source="Tiled/Glass 5.png"/>
 </tile>
 <tile id="29">
  <image width="250" height="383" source="Tiled/Goal.png"/>
 </tile>
</tileset>
