﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow_mouse_intro : MonoBehaviour
{
    private Vector3 target;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.position = new Vector3(target.x, target.y, transform.position.z);
    }
}
