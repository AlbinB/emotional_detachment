﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button_script : MonoBehaviour
{
    public GameObject[] doors; // Array (list) of all the doors to be controlled
    public GameObject[] platforms; // Array (list) of all the moving platforms to be controlled
    //private float distance;
    private Vector3 pos;
    private Vector3 scale;

    public AudioSource audioSource;
    public AudioClip button_sound;

    public bool contact = false;

    public bool active = false;
    private SpriteRenderer spriteRenderer;
    public Sprite b_active;
    public Sprite b_inactive;


    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (contact)
        {
            spriteRenderer.sprite = b_active;
        }
        else
        {
            spriteRenderer.sprite = b_inactive;
        }
        //Vector3.Distance(gameObject.transform.position, clone_controller.active_player.transform.position);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!contact)
        {
            audioSource.PlayOneShot(button_sound, 0.5F);
            contact = true;
            door_handling();
            platform_handling();
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (contact)
        {
            contact = false;
            door_handling();
            platform_handling();
        }
    }
    void door_handling()
    {
        for (int i = 0; i < doors.Length; i++)
        {
            if (doors[i].GetComponent<door_script>().open == true)
            {
                doors[i].GetComponent<door_script>().open = false;
            }
            else
            {
                doors[i].GetComponent<door_script>().open = true;
            }
        }
    }

    void platform_handling()
    {
        for (int i = 0; i < platforms.Length; i++)
        {
            if (platforms[i].GetComponent<MovingPlatform>().activated == true)
            {
                platforms[i].GetComponent<MovingPlatform>().activated = false;
            }
            else
            {
                platforms[i].GetComponent<MovingPlatform>().activated = true;
            }
        }
    }

}